<?php

use Illuminate\Database\Seeder;

use App\Models\Sede as Sede;

class SedeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Sede::create(['id'=> '1','nomSede' => 'Bogotá']);
      Sede::create(['id'=> '2','nomSede' => 'México']);
      Sede::create(['id'=> '3','nomSede' => 'Perú']);
    }
}
