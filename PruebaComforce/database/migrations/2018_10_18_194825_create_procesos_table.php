<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcesosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procesos', function (Blueprint $table) {
            $table->increments('id');
            $table->String('numProceso', 10)->unique();
            $table->String('desProceso', 200);
            $table->Date('feccreProceso');
            $table->decimal('preProceso',15, 2);
            $table->decimal('predolProceso',15, 2);
            $table->integer('idSede')->unsigned();
            $table->foreign('idSede')->references('id')->on('sedes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('procesos');
    }
}
