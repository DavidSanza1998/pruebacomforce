<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sede extends Model
{
    protected $table = "sedes";
    protected $fillable = ['nomSede'];
    protected $guarded = ['id'];
}
