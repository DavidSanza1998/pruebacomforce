<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Proceso as Proceso;

use App\Models\Sede as Sede;

use Illuminate\Support\Facades\Input;

class ProcesoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $procesos = Proceso::select('sedes.*','procesos.*')
                            ->join('sedes', 'sedes.id', '=', 'procesos.idSede')
                            ->get();

        return view("Proceso/list", compact("procesos"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $sedes = Sede::select('sedes.*')->get();

      return view("Proceso/create", compact("sedes"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $procesos = new Proceso;
      $procesos->numProceso = $request->numProceso;
      $procesos->desProceso = $request->desProceso;
      $procesos->feccreProceso = date('Y/m/d');
      $resultado_punto = str_replace(".", "", $request->preProceso);
      $resultado = str_replace(",", ".", $resultado_punto);
      $procesos->preProceso = $resultado;
      $procesos->predolProceso = $resultado / 3092.90;
      $procesos->idSede = $request->idSede;

      $procesos->save();

      return redirect('procesos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $procesos = Proceso::find($id);

        $sedes = Sede::select('sedes.*')->get();

        return view("Proceso/update", compact("procesos","sedes"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $procesos = Proceso::find($id);
        $procesos->numProceso = $request->numProceso;
        $procesos->desProceso = $request->desProceso;
        $resultado_punto = str_replace(".", "", $request->preProceso);
        $resultado = str_replace(",", ".", $resultado_punto);
        $procesos->preProceso = $resultado;
        $procesos->predolProceso = $resultado / 3092.90;
        $procesos->idSede = $request->idSede;

        $procesos->save();

        return redirect('procesos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $procesos = Proceso::find($id)->delete();

        $notification = array(
          'message' => 'Se elimino exitosamente el Proceso',
          'alert-type' => 'success'
        );

        return redirect('procesos')->with($notification);
    }

    public function ajax_validacion_numero_unico()
    {
      $numero_proceso = Input::get('numProceso');

      $id = Input::get('id');

      $valida_numero = Proceso::select('procesos.*')
                            ->where('procesos.numProceso', '=' , $numero_proceso)
                            ->where('procesos.id', '!=' , $id)
                            ->get();

      return response()->json($valida_numero);
    }
}
