var pathname = $("meta[name=pathname]").attr("content");
/*
|--------------------------------------------------------------------------
| VALIDACIONES CLIMAS
|--------------------------------------------------------------------------
*/
$('#form_procesos').submit(function(event) {
  var response = true;

  if ($('#numProceso').val() == '') {
    toastr.error("El campo Nombre es obligatorio", "Error");
    response = false;
  }
  if ($('#desProceso').val() == '') {
    toastr.error("El campo Descripcion es obligatorio", "Error");
    response = false;
  }
  if ($('#preProceso').val() == '') {
    toastr.error("El campo Presupuesto es obligatorio", "Error");
    response = false;
  }

  if ($('#idSede').val() == '') {
    toastr.error("El campo Sede es obligatorio", "Error");
    response = false;
  }

  var numProceso =  $('#numProceso').val();
  var id =  $('#id').val();
  var token = $("#token").val();

  var parametros = {
      "numProceso":numProceso,
      "id":id,
    }

  $.ajax({
    url: pathname+'/ajax_validacion_numero_unico?numProceso=' + numProceso,
    type: 'get',
    async:false,
    data:parametros,
    headers: {'X-CSRF-TOKEN': token},
    success: function(data){
      if(data.length != 0){
        toastr.error("Este Numero de Proceso ya se encuentra registrado.", "Error");
        response = false;
      }
       if(data.length == 0){
       return true;
      }
    },
  });

  var caracteres_numero = $('#numProceso').val();
  if(caracteres_numero.length != 8){
    console.log(caracteres_numero.length);
    toastr.error("El campo Numero Proceso debe tener 8 caracteres", "Error");
    response = false;
  }

  var caracteres_descripcion = $('#desProceso').val();
  if(caracteres_descripcion.length > 200){
    toastr.error("El campo Descripcion tiene mas de 200 caracteres", "Error");
    response = false;
  }

  return response;
});
