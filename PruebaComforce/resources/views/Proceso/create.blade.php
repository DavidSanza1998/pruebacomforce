@extends('layouts.app')
  @section('content')
    <h1 class="col-md-offset-4">Agregar Proceso</h1>
    <input type="hidden" name="_token" value="{{ csrf_token()}}" id="token">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        {!! Form::open(['route' => 'procesos.store', 'id' => 'form_procesos']) !!}
          <div class="form-group">
            <label for="exampleInputEmail1">Numero Proceso</label>
            <input type="text" name="numProceso" class="form-control" id="numProceso" placeholder="Numero Proceso">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Descripcion</label>
            <textarea name="desProceso" id="desProceso" placeholder="Descripcion" class="form-control" rows="3"></textarea>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Presupuesto</label>
            <input type="text" name="preProceso" class="form-control" id="preProceso" placeholder="Presupuesto">
          </div>
          <div class="form-group">
            <select name="idSede" id="idSede" class="form-control">
              <option value="" readonly selected>Seleccione la Sede</option>
              @foreach($sedes as $sede)
                <option value="{{$sede->id}}">{{$sede->nomSede}}</option>
              @endforeach
            </select>
          </div>
          <button type="submit" class="btn btn-success"><i class="icon-plus-sign icon-large" aria-hidden="true"></i> Guardar</button>
        {!! Form::close() !!}
      </div>
    </div>
    <script src="{{ asset('js/proceso.js') }}"></script>
    <script>
      $('#preProceso').mask('0.000.000.000.000,00', {reverse: true});
    </script>
  @endsection
