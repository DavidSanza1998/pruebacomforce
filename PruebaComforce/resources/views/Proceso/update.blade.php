@extends('layouts.app')
  @section('content')
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        {!! Form::model($procesos, ['route' => ['procesos.update', $procesos->id],'method' => 'PUT', 'id' => 'form_procesos']) !!}
          <input type="hidden" id="id" value="{{$procesos->id}}">
          <h1 class="col-md-offset-1">Actualizar Proceso Numero {{$procesos->numProceso}}</h1>
          <div class="form-group">
            <label for="exampleInputPassword1">Fecha Creacion</label>
            <input type="text" value="{{$procesos->feccreProceso}}" name="feccreProceso" class="form-control" placeholder="Presupuesto" disabled>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Numero Proceso</label>
            <input type="text" value="{{$procesos->numProceso}}" name="numProceso" class="form-control" id="numProceso" placeholder="Numero Proceso">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Descripcion</label>
            <textarea name="desProceso" id="desProceso" placeholder="Descripcion" class="form-control" rows="3">{{$procesos->desProceso}}</textarea>
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Presupuesto</label>
            <input type="text" value="{{$procesos->preProceso}}" name="preProceso" class="form-control" id="preProceso" placeholder="Presupuesto">
          </div>
          <div class="form-group">
            <select name="idSede" id="idSede" class="form-control">
              <option value="" readonly selected>Seleccione la Sede</option>
              @foreach($sedes as $sede)
                @if($procesos->idSede == $sede->id)
                  <option selected value="{{$sede->id}}">{{$sede->nomSede}}</option>
                @else
                  <option value="{{$sede->id}}">{{$sede->nomSede}}</option>
                @endif
              @endforeach
            </select>
          </div>
          <button type="submit" class="btn btn-success"><i class="icon-pencil icon-large"></i> Actualizar</button>
        {!! Form::close() !!}
      </div>
    </div>
    <script>
      $('#preProceso').mask('0.000.000.000.000,00', {reverse: true});
    </script>
    <script src="{{ asset('js/proceso.js') }}"></script>
  @endsection
