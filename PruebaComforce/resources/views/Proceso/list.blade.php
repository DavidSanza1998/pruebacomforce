@extends('layouts.app')
  @section('content')
  <h1 class="col-md-offset-4">Lista de Procesos</h1>
  @if ($errors->any())
      <div class=" col-md-offset-2 separacion alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error}}</li>
          @endforeach
        </ul>
      </div>
    @endif
    <div class="row">
      <div class="col-md-2 col-md-offset-9" style="margin-bottom: 1%;">
        <a href="{{route('procesos.create')}}" class="btn btn-success pull-right"><i class="icon-plus-sign icon-large" aria-hidden="true"></i> Crear Proceso</a>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <table class="table table-striped table-hover" id="tabla_proceso">
          <thead style="text-align:center;">
            <tr>
              <th>Fecha Creacion</th>
              <th>Consecutivo</th>
              <th>Numero Proceso</th>
              <th>Ver Datos</th>
              <th>Editar</th>
              <th>Eliminar</th>
            </tr>
          </thead>
          <tbody>
            @foreach($procesos as $proceso)
              <tr>
                <td>{{$proceso->feccreProceso}}</td>
                <td>{{$proceso->id}}</td>
                <td>{{$proceso->numProceso}}</td>
                <td>
                  <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal{{$proceso->id}}">
                    <i class="icon-eye-open icon-large"></i>
                  </button>
                </td>
                <td>
                  <a href="{{ route('procesos.edit', $proceso->id) }}" class="btn btn-primary"><i class="icon-pencil icon-large"></i></a>
                </td>
                <td>
                  {!! Form::open(['route' => ['procesos.destroy', $proceso->id], 'method' => 'DELETE']) !!}
                    <button class="btn btn-danger"><i class="icon-trash icon-large"></i></button>
                  {!! Form::close() !!}
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <!-- Modal -->
    @foreach($procesos as $proceso)
      <div class="modal fade" id="myModal{{$proceso->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Todos los datos del proceso {{$proceso->id}}</h4>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Consecutivo</label>
                    <input type="text" class="form-control" value="{{$proceso->id}}" placeholder="Presupuesto" disabled>
                  </div>
                </div>
                <div class="col-md-5 col-md-offset-1">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Fecha Creacion</label>
                    <input type="text" class="form-control" value="{{$proceso->feccreProceso}}" placeholder="Presupuesto" disabled>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Numero Proceso</label>
                    <input type="text" class="form-control" value="{{$proceso->numProceso}}" placeholder="Numero Proceso" disabled>
                  </div>
                </div>
                <div class="col-md-5 col-md-offset-1">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Descripcion</label>
                    <input type="text" class="form-control" value="{{$proceso->desProceso}}" placeholder="Descripcion" disabled>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Presupuesto Pesos Colombianos</label>
                    <input type="text" class="form-control moneda" value="{{$proceso->preProceso}}" placeholder="Presupuesto Pesos Colombianos" disabled>
                  </div>
                </div>
                <div class="col-md-5 col-md-offset-1">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Presupuesto Dolares</label>
                    <input type="text" class="form-control moneda" value="{{$proceso->predolProceso}}" placeholder="Presupuesto Pesos Colombianos" disabled>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-5 col-md-offset-3">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Sede</label>
                    <input type="text" class="form-control" value="{{$proceso->nomSede}}" placeholder="Sede" disabled>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <script>
        $('.moneda').mask('0.000.000.000.000,00', {reverse: true});
      </script>
    @endforeach
    <script>
      $(document).ready( function () {
        $('#tabla_proceso').DataTable({
          "language": {
						    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
						}
        });
      });
    </script>
  @endsection
