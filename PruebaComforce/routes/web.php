<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {

  Route::get('/home', 'HomeController@index')->name('home');

  //RUTAS PARA LOS PROCESO DEL SISTEMA
  Route::get('/procesos', 'ProcesoController@index')->name('procesos.index');

  Route::get('/procesos/create', 'ProcesoController@create')->name('procesos.create');

  Route::post('/procesos/store', 'ProcesoController@store')->name('procesos.store');

  Route::get('procesos/{role}/edit', 'ProcesoController@edit')->name('procesos.edit');

	Route::put('procesos/{role}', 'ProcesoController@update')->name('procesos.update');

	Route::delete('procesos/{role}', 'ProcesoController@destroy')->name('procesos.destroy');

  //RUTAS DE AJAX
  Route::get('/ajax_validacion_numero_unico', 'ProcesoController@ajax_validacion_numero_unico');

});
